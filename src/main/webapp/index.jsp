<%-- 
    Document   : index
    Created on : 07-07-2021, 02:53:36
    Author     : Juan Ramirez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Evaluacion 3 CRUD (id=nombreempresa)</h1>
        
        <h1>GET Lista = (https://app-sol3.herokuapp.com/api/clientes) MOSTRARA LISTA</h1>
        
        <h1>POST Crear = (https://app-sol3.herokuapp.com/api/clientes)INGRESAR DATOS</h1>
        
        <h1>GET Buscar Por Id =(https://app-sol3.herokuapp.com/api/clientes"+/ID")BUSCARA POR ID </h1>
        
        <h1>PUT Actualizar =(https://app-sol3.herokuapp.com/api/clientes)MODIFICAR DATOS </h1>
        
        <h1>Delete = (https://app-sol3.herokuapp.com/api/clientes"+/ID")BORRARA POR ID  </h1>
        
    </body>
</html>
