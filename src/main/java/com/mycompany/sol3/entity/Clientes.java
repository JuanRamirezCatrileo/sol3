/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sol3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Juan Ramirez
 */
@Entity
@Table(name = "clientes")
@NamedQueries({
    @NamedQuery(name = "Clientes.findAll", query = "SELECT c FROM Clientes c"),
    @NamedQuery(name = "Clientes.findByNombreEmpresa", query = "SELECT c FROM Clientes c WHERE c.nombreEmpresa = :nombreEmpresa"),
    @NamedQuery(name = "Clientes.findByCantidad", query = "SELECT c FROM Clientes c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "Clientes.findByValorUnitario", query = "SELECT c FROM Clientes c WHERE c.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "Clientes.findByValorTotal", query = "SELECT c FROM Clientes c WHERE c.valorTotal = :valorTotal")})
public class Clientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombreempresa")
    private String nombreEmpresa;
    @Size(max = 2147483647)
    @Column(name = "cantidad")
    private String cantidad;
    @Size(max = 2147483647)
    @Column(name = "valorunitario")
    private String valorUnitario;
    @Size(max = 2147483647)
    @Column(name = "valortotal")
    private String valorTotal;

    public Clientes() {
    }

    public Clientes(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(String valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombreEmpresa != null ? nombreEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clientes)) {
            return false;
        }
        Clientes other = (Clientes) object;
        if ((this.nombreEmpresa == null && other.nombreEmpresa != null) || (this.nombreEmpresa != null && !this.nombreEmpresa.equals(other.nombreEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.sol3.entity.Clientes[ nombreEmpresa=" + nombreEmpresa + " ]";
    }
    
}
