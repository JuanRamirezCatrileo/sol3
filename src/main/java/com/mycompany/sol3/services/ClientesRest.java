/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sol3.services;

import com.mycompany.sol3.dao.ClientesJpaController;
import com.mycompany.sol3.dao.exceptions.NonexistentEntityException;
import com.mycompany.sol3.entity.Clientes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Juan Ramirez
 */
@Path("clientes")

public class ClientesRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response listarClientes() {

        ClientesJpaController dao = new ClientesJpaController();

        List<Clientes> clientes = dao.findClientesEntities();

        return Response.ok(200).entity(clientes).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Clientes clientes) {

        ClientesJpaController dao = new ClientesJpaController();
        try {
            dao.create(clientes);
        } catch (Exception ex) {
            Logger.getLogger(ClientesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(clientes).build();
        
    }
@PUT
@Produces(MediaType.APPLICATION_JSON)
   public Response actualizar (Clientes clientes){
       
       ClientesJpaController dao = new ClientesJpaController();
       
        try {
            dao.edit(clientes);
        } catch (Exception ex) {
            Logger.getLogger(ClientesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok(200).entity(clientes).build();
   } 
   
   @DELETE
   @Path("/{ideliminar}")
  @Produces (MediaType.APPLICATION_JSON)
    
    public Response eliminar(@PathParam("ideliminar") String ideliminar){
        ClientesJpaController dao = new ClientesJpaController();
        
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ClientesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Cliente Eliminado").build();
    }
  @GET
  @Path("/{idConsultar}")
  @Produces (MediaType.APPLICATION_JSON)
  public Response consultarPorId(@PathParam("idConsultar") String idConsultar){
      ClientesJpaController dao = new ClientesJpaController();
      Clientes clientes = dao.findClientes(idConsultar);
      
      return Response.ok(200).entity(clientes).build();
  }  
}
